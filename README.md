# README #

This README documents required steps to install, configure and use the PureCloud Email Screen Pop extension for Firefox.

### What does PureCloud Email Screen Pop do? ###

* Screen pop a new tab on a defined URL when an agent answers an ACD email in PureCloud
* This extension is for **Firefox only**

### How do I set up the PureCloud Email Screen extension? ###

* This extension is for **Firefox only**
* Use Firefox Developer Edition
* Download the PureCloudEmailScreenPop folder
* Open Firefox Developer Edition and go to about:debugging
* Temporary install the extension by importing the manifest.json
* Go to about:addons to configure the extension parameters
* Select your PureCloud region
* Enter the base URL to use for screen pop
* Save options
* Log into PureCloud
* Your browser is now listening for email pickup notifications

### Default screen pop ###

* Open a new tab in Firefox on email answering
* URL is the base URL set in extension options

* Parameters passed along to the base URL:
* conversationId
* emailSubject
* senderName
* senderAddress
* emailReceivedDate
* emailReceivedDate
* recipientAddress
* agentName
* pickupDate
* queueId