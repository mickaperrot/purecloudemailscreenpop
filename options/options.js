// Save options to storage
function saveOptions() {
  var settingItem = browser.storage.local.set({
    environment: document.querySelector("#environment").value,
    url: document.querySelector("#url").value
  })
    .then(null)
    .catch(function(){
      console.log("Error saving options to storage");
    });
}

// Retrieve options from storage
function restoreOptions() {
  var gettingItem = browser.storage.local.get(["environment" ,"url"])
    .then((res) => {
      document.querySelector("#environment").value = res.environment;
      document.querySelector("#url").value = res.url;
    })
    .catch(function() {
      console.log("Error loading options from storage");
    });
}

// Restore options on document load
document.addEventListener('DOMContentLoaded', restoreOptions);
// Save option on submit
document.querySelector("form").addEventListener("submit", saveOptions);
