// Get environment value from local storage
function getEnvironmentFromStorage(){
  browser.storage.local.get("environment")
    .then(function(res){
      if(Object.keys(res).length == 0){
        // Environment not set in storage, open options
        console.log("Environment not set in storage");
        // Change browser action icon and tooltip
        browser.browserAction.setIcon({path: "icons/inactiveLogo19.png"});
        browser.browserAction.setTitle({title: "No environment set in options"});
      }
      else{
        // Environment found in storage
        console.log("Environment found in storage: " + res.environment);
        getTokenFromCookie(res.environment);
      }
    });
}

// Get and test auth token from cookie
function getTokenFromCookie(environment){
  // Get cookie
  browser.cookies.get({
    url: "https://apps." + environment,
    name: "ININ-Auth-Api"
  }).then(function(cookie){
    if(cookie){
      // Token is available
      console.log("Got token from cookie: " + cookie.value);
      // Check token validity
      var session = new purecloud.platform.PureCloudSession({
        environment: environment,
        strategy: 'token',
        token: cookie.value
      });
      var usersApi = new purecloud.platform.UsersApi(session);
      usersApi.getMe()
        .then(function(me){
          // Token is valid
          console.log("Token is valid for user: " + me.id);
          subscribeNotifications(session, me.id);
        })
        .catch(function(){
          console.log("Token is not valid anymore");
          // Change browser action icon and tooltip
          browser.browserAction.setIcon({path: "icons/inactiveLogo19.png"});
          browser.browserAction.setTitle({title: "Not logged into PureCloud"});
        });
    }
    else{
      // No token found in cookie
      console.log("No token found in cookies");
      // Change browser action icon and tooltip
      browser.browserAction.setIcon({path: "icons/inactiveLogo19.png"});
      browser.browserAction.setTitle({title: "Not logged into PureCloud"});
    }
  });
}

// Subscribe to email pickup notification for current user and listen to notifications
function subscribeNotifications(session, userId){
  console.log("Subscribing to notifications");
  var notifications = new purecloud.platform.NotificationsApi(session);
  // Posting a new channel
  //*** 10 channels per org limit! ***
  notifications.postChannels()
    .then(function(result){
      // Posting subscription
      var subscriptionTopic = "v2.users." + userId + ".conversations.emails";
      notifications.putChannelsChannelIdSubscriptions(result.id, [{"id": subscriptionTopic}])
        .then(function(){
          console.log("Topic " + subscriptionTopic + " successfully suscribed on: " + result.connectUri);
          // Connect to WebSocket
          var socket = new WebSocket(result.connectUri);
          socket.onopen = function(){
            console.log("WebSocket open, listening for notification...");
            // Change browser action icon and tooltip
            browser.browserAction.setIcon({path: "/icons/logo19.png"});
            browser.browserAction.setTitle({title: "Listening for notifications..."});
          };
          // Listen to WebSocket
          socket.onmessage = function(event){
            var notification = JSON.parse(event.data);
            // Filter notification events
            if(notification.topicName == subscriptionTopic){
              notification.eventBody.participants.forEach(function(participant){
                if(participant.purpose == "agent" && participant.user.id == userId && participant.connectedTime && !participant.endTime){
                  console.log("Email conversation picked-up: " + notification.eventBody.id);
                  handleNotification(notification);
                }
              });
            }
          };
        })
        .catch(function(error){
          console.log("Error posting subscription");
          // Change browser action icon and tooltip
          browser.browserAction.setIcon({path: "icons/inactiveLogo19.png"});
          browser.browserAction.setTitle({title: "Error subscribing to notifications"});
        });
    })
    .catch(function(error){
      console.log("Error posting channel");
      // Change browser action icon and tooltip
      browser.browserAction.setIcon({path: "icons/inactiveLogo19.png"});
      browser.browserAction.setTitle({title: "Error subscribing to notifications"});
    });
};

function listenCookieChange(changeInfo){
  if(changeInfo.cookie.name == 'ININ-Auth-Api' && changeInfo.cause == 'explicit'){
    console.log("New token found in cookie: " + changeInfo.cookie.value);
    getEnvironmentFromStorage();
  }
}

function listenStorageChange(changes, area){
  if(changes.hasOwnProperty('environment')){
    console.log("Environment value changed from " + changes.environment.oldValue + " to " + changes.environment.newValue);
    getEnvironmentFromStorage();
  }
}

// Listen to action click and try to authenticate
browser.browserAction.onClicked.addListener(getEnvironmentFromStorage)

// Listen cookie change for new token
browser.cookies.onChanged.addListener(listenCookieChange);

// Listen storage change for new environment
browser.storage.onChanged.addListener(listenStorageChange)

// Try to authenticate at startup
browser.runtime.onStartup.addListener(getEnvironmentFromStorage);
