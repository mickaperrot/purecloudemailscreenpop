// Triggered when agent picks up an email
function handleNotification(notification){
  var conversationId = notification.eventBody.id;
  var emailSubject;
  var senderName;
  var senderAddress;
  var emailReceivedDate;
  var recipientAddress;
  var agentName;
  var pickupDate;
  var queueId;
  // Retreive user ID from notification topic in case there are multiple agents on the conversation
  var re = /v2\.users\.([^\.]+)/i;
  var participantId = notification.topicName.match(re);
  // Loop participants in the conversation
  notification.eventBody.participants.forEach(function(participant){
    // Get customer details
    if(participant.purpose == "customer"){
      emailSubject = participant.subject;
      senderName = participant.name;
      senderAddress = participant.address;
      emailReceivedDate = participant.connectedTime;
    }
    // Get agent details
    if(participant.purpose == "agent" && participant.user.id == participantId[1]){
      recipientAddress = participant.address;
      agentName = participant.name;
      pickupDate = participant.connectedTime;
      queueId = participant.queue.id;
    }
  });
  // Get URL from storage
  browser.storage.local.get("url").then(function(res){
    if(res.url){
      // Construct screen pop URL
      url = res.url + encodeURI("?conversationId=" + encodeURIComponent(conversationId) + "&emailSubject=" + encodeURIComponent(emailSubject) + "&senderName=" + encodeURIComponent(senderName) + "&senderAddress=" + encodeURIComponent(senderAddress) + "&emailReceivedDate=" + encodeURIComponent(emailReceivedDate) + "&emailReceivedDate=" +
      encodeURIComponent(emailReceivedDate) + "&recipientAddress=" + encodeURIComponent(recipientAddress) + "&agentName=" + encodeURIComponent(agentName) + "&pickupDate=" + encodeURIComponent(pickupDate) + "&queueId=" + encodeURIComponent(queueId));
      // Open the URL in a new tab
      console.log("Opening tab: " + url);
      browser.tabs.create({
        "url": url
      });
    }
    else {
      console.log("URL in storage is empty");
    }
  }, function(){
    console.log("Error retrieving screen pop URL from storage");
  });
};
